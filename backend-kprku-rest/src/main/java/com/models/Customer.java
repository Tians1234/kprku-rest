package com.models;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="customer")
public class Customer {

	String id;
	String namaDepan;
	String namaTengah;
	String namaBelakang;
	String nomorKtp;
	String tempatLahir;
	Date tglLahir;
	String noTelepon;
	String noHp;
	String email;
	Boolean isMenikah;
	String jenisKelamin;
	Integer jumlahAnak;
	String idPropinsiCustomer;
	String idKabupatenCustomer;
	String idKecamatanCustomer;
	String idKelurahanCustomer;
	String kodePosCustomer;
	Date createdDate;
	Date updatedDate;
	Boolean isDelete;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNamaDepan() {
		return namaDepan;
	}
	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}
	public String getNamaTengah() {
		return namaTengah;
	}
	public void setNamaTengah(String namaTengah) {
		this.namaTengah = namaTengah;
	}
	public String getNamaBelakang() {
		return namaBelakang;
	}
	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}
	public String getNomorKtp() {
		return nomorKtp;
	}
	public void setNomorKtp(String nomorKtp) {
		this.nomorKtp = nomorKtp;
	}
	public String getTempatLahir() {
		return tempatLahir;
	}
	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}
	public Date getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(Date tglLahir) {
		this.tglLahir = tglLahir;
	}
	public String getNoTelepon() {
		return noTelepon;
	}
	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}
	public String getNoHp() {
		return noHp;
	}
	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getIsMenikah() {
		return isMenikah;
	}
	public void setIsMenikah(Boolean isMenikah) {
		this.isMenikah = isMenikah;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public Integer getJumlahAnak() {
		return jumlahAnak;
	}
	public void setJumlahAnak(Integer jumlahAnak) {
		this.jumlahAnak = jumlahAnak;
	}
	public String getIdPropinsiCustomer() {
		return idPropinsiCustomer;
	}
	public void setIdPropinsiCustomer(String idPropinsiCustomer) {
		this.idPropinsiCustomer = idPropinsiCustomer;
	}
	public String getIdKabupatenCustomer() {
		return idKabupatenCustomer;
	}
	public void setIdKabupatenCustomer(String idKabupatenCustomer) {
		this.idKabupatenCustomer = idKabupatenCustomer;
	}
	public String getIdKecamatanCustomer() {
		return idKecamatanCustomer;
	}
	public void setIdKecamatanCustomer(String idKecamatanCustomer) {
		this.idKecamatanCustomer = idKecamatanCustomer;
	}
	public String getIdKelurahanCustomer() {
		return idKelurahanCustomer;
	}
	public void setIdKelurahanCustomer(String idKelurahanCustomer) {
		this.idKelurahanCustomer = idKelurahanCustomer;
	}
	public String getKodePosCustomer() {
		return kodePosCustomer;
	}
	public void setKodePosCustomer(String kodePosCustomer) {
		this.kodePosCustomer = kodePosCustomer;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
	
}
