package com.models;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="units")
public class Units {

	String id;
	String idProperty;
	String idDeveloper;
	String blok;
	String nomor;
	Integer kelebihanTanah;
	Integer hargaPermeter;
	Integer hargaJual;
	Integer hargaExtraStrategis;
	String keteranganStrategis;
	Integer hargaBookingFee;
	Boolean isSecond;
	Boolean isSold;
	Date soldDate;
	Date createdDate;
	Date updatedDate;
	Boolean isDelete;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdProperty() {
		return idProperty;
	}
	public void setIdProperty(String idProperty) {
		this.idProperty = idProperty;
	}
	public String getIdDeveloper() {
		return idDeveloper;
	}
	public void setIdDeveloper(String idDeveloper) {
		this.idDeveloper = idDeveloper;
	}
	public String getBlok() {
		return blok;
	}
	public void setBlok(String blok) {
		this.blok = blok;
	}
	public String getNomor() {
		return nomor;
	}
	public void setNomor(String nomor) {
		this.nomor = nomor;
	}
	public Integer getKelebihanTanah() {
		return kelebihanTanah;
	}
	public void setKelebihanTanah(Integer kelebihanTanah) {
		this.kelebihanTanah = kelebihanTanah;
	}
	public Integer getHargaPermeter() {
		return hargaPermeter;
	}
	public void setHargaPermeter(Integer hargaPermeter) {
		this.hargaPermeter = hargaPermeter;
	}
	public Integer getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(Integer hargaJual) {
		this.hargaJual = hargaJual;
	}
	public Integer getHargaExtraStrategis() {
		return hargaExtraStrategis;
	}
	public void setHargaExtraStrategis(Integer hargaExtraStrategis) {
		this.hargaExtraStrategis = hargaExtraStrategis;
	}
	public String getKeteranganStrategis() {
		return keteranganStrategis;
	}
	public void setKeteranganStrategis(String keteranganStrategis) {
		this.keteranganStrategis = keteranganStrategis;
	}
	public Integer getHargaBookingFee() {
		return hargaBookingFee;
	}
	public void setHargaBookingFee(Integer hargaBookingFee) {
		this.hargaBookingFee = hargaBookingFee;
	}
	public Boolean getIsSecond() {
		return isSecond;
	}
	public void setIsSecond(Boolean isSecond) {
		this.isSecond = isSecond;
	}
	public Boolean getIsSold() {
		return isSold;
	}
	public void setIsSold(Boolean isSold) {
		this.isSold = isSold;
	}
	public Date getSoldDate() {
		return soldDate;
	}
	public void setSoldDate(Date soldDate) {
		this.soldDate = soldDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
}
