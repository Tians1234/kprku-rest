package com.models;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="bank")
public class Bank {

	String id;
	String namaBank;
	String profileSingkat;
	String cabangBank;
	Boolean isSyariah;
	String rtBank;
	String rwBank;
	String idPropinsiBank;
	String idKabupatenBank;
	String idKecamatanBank;
	String idKelurahanBank;
	String kodePosBank;
	String uploadCompanyLogo;
	String noTelepon;
	String noHp;	
	@Email(message="email salah")
	String email;
	Date createdDate;
	Date updatedDate;
	Boolean isDelete;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNamaBank() {
		return namaBank;
	}
	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}
	public String getProfileSingkat() {
		return profileSingkat;
	}
	public void setProfileSingkat(String profileSingkat) {
		this.profileSingkat = profileSingkat;
	}
	public String getCabangBank() {
		return cabangBank;
	}
	public void setCabangBank(String cabangBank) {
		this.cabangBank = cabangBank;
	}
	public Boolean getIsSyariah() {
		return isSyariah;
	}
	public void setIsSyariah(Boolean isSyariah) {
		this.isSyariah = isSyariah;
	}
	public String getRtBank() {
		return rtBank;
	}
	public void setRtBank(String rtBank) {
		this.rtBank = rtBank;
	}
	public String getRwBank() {
		return rwBank;
	}
	public void setRwBank(String rwBank) {
		this.rwBank = rwBank;
	}
	public String getIdPropinsiBank() {
		return idPropinsiBank;
	}
	public void setIdPropinsiBank(String idPropinsiBank) {
		this.idPropinsiBank = idPropinsiBank;
	}
	public String getIdKabupatenBank() {
		return idKabupatenBank;
	}
	public void setIdKabupatenBank(String idKabupatenBank) {
		this.idKabupatenBank = idKabupatenBank;
	}
	public String getIdKecamatanBank() {
		return idKecamatanBank;
	}
	public void setIdKecamatanBank(String idKecamatanBank) {
		this.idKecamatanBank = idKecamatanBank;
	}
	public String getIdKelurahanBank() {
		return idKelurahanBank;
	}
	public void setIdKelurahanBank(String idKelurahanBank) {
		this.idKelurahanBank = idKelurahanBank;
	}
	public String getKodePosBank() {
		return kodePosBank;
	}
	public void setKodePosBank(String kodePosBank) {
		this.kodePosBank = kodePosBank;
	}
	public String getUploadCompanyLogo() {
		return uploadCompanyLogo;
	}
	public void setUploadCompanyLogo(String uploadCompanyLogo) {
		this.uploadCompanyLogo = uploadCompanyLogo;
	}
	public String getNoTelepon() {
		return noTelepon;
	}
	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}
	public String getNoHp() {
		return noHp;
	}
	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}
