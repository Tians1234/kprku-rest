package com.models;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="developer")
public class Developer {

	String id;
	String namaDeveloper;
	String profileSingkat;
	String alamatDeveloper;
	String rtDeveloper;
	String rwDeveloper;
	String idPropinsiDeveloper;
	String idKabupatenDeveloper;
	String idKecamatanDeveloper;
	String idKelurahanDeveloper;
	String kodePosDeveloper;
	String uploadCompanyLogo;
	String noTelepon;
	String noHp;
	String email;
	Date createdDate;
	Date updatedDate;
	Boolean isDelete;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
	public String getNamaDeveloper() {
		return namaDeveloper;
	}
	public void setNamaDeveloper(String namaDeveloper) {
		this.namaDeveloper = namaDeveloper;
	}
	public String getProfileSingkat() {
		return profileSingkat;
	}
	public void setProfileSingkat(String profileSingkat) {
		this.profileSingkat = profileSingkat;
	}
	public String getAlamatDeveloper() {
		return alamatDeveloper;
	}
	public void setAlamatDeveloper(String alamatDeveloper) {
		this.alamatDeveloper = alamatDeveloper;
	}
	public String getRtDeveloper() {
		return rtDeveloper;
	}
	public void setRtDeveloper(String rtDeveloper) {
		this.rtDeveloper = rtDeveloper;
	}
	public String getRwDeveloper() {
		return rwDeveloper;
	}
	public void setRwDeveloper(String rwDeveloper) {
		this.rwDeveloper = rwDeveloper;
	}
	public String getIdPropinsiDeveloper() {
		return idPropinsiDeveloper;
	}
	public void setIdPropinsiDeveloper(String idPropinsiDeveloper) {
		this.idPropinsiDeveloper = idPropinsiDeveloper;
	}
	public String getIdKabupatenDeveloper() {
		return idKabupatenDeveloper;
	}
	public void setIdKabupatenDeveloper(String idKabupatenDeveloper) {
		this.idKabupatenDeveloper = idKabupatenDeveloper;
	}
	public String getIdKecamatanDeveloper() {
		return idKecamatanDeveloper;
	}
	public void setIdKecamatanDeveloper(String idKecamatanDeveloper) {
		this.idKecamatanDeveloper = idKecamatanDeveloper;
	}
	public String getIdKelurahanDeveloper() {
		return idKelurahanDeveloper;
	}
	public void setIdKelurahanDeveloper(String idKelurahanDeveloper) {
		this.idKelurahanDeveloper = idKelurahanDeveloper;
	}
	public String getKodePosDeveloper() {
		return kodePosDeveloper;
	}
	public void setKodePosDeveloper(String kodePosDeveloper) {
		this.kodePosDeveloper = kodePosDeveloper;
	}
	public String getUploadCompanyLogo() {
		return uploadCompanyLogo;
	}
	public void setUploadCompanyLogo(String uploadCompanyLogo) {
		this.uploadCompanyLogo = uploadCompanyLogo;
	}
	public String getNoTelepon() {
		return noTelepon;
	}
	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}
	public String getNoHp() {
		return noHp;
	}
	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
}
