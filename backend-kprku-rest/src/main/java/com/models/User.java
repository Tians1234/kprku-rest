package com.models;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="user")
public class User {

	String id;
	String role;
	String username;
	String password;
	String idDeveloper;
	String idBank;
	String idKjpp;
	String idNotaris;
	String idAsuransi;	
	Date createdDate;
	Date updateDate;
	Boolean isDelete;	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIdDeveloper() {
		return idDeveloper;
	}
	public void setIdDeveloper(String idDeveloper) {
		this.idDeveloper = idDeveloper;
	}
	public String getIdBank() {
		return idBank;
	}
	public void setIdBank(String idBank) {
		this.idBank = idBank;
	}
	public String getIdKjpp() {
		return idKjpp;
	}
	public void setIdKjpp(String idKjpp) {
		this.idKjpp = idKjpp;
	}
	public String getIdNotaris() {
		return idNotaris;
	}
	public void setIdNotaris(String idNotaris) {
		this.idNotaris = idNotaris;
	}
	public String getIdAsuransi() {
		return idAsuransi;
	}
	public void setIdAsuransi(String idAsuransi) {
		this.idAsuransi = idAsuransi;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
	
}
