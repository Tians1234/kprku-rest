package com.models;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="pembelian")
public class Pembelian {

	String id;
	String idCustomer;
	String idDeveloper;
	String idBank;
	Integer pengajuanDp;
	Integer pengajuanAngsuran;
	Integer pengajuanTenor;
	Boolean isPesanNup;
	Boolean isBerkasLengkap;
	Boolean isAnalisaDisetujui;
	Boolean isBiayaKprLunas;
	Boolean isAkad;
	Date tglPesanNup;
	Date tglBerkasMasuk;
	Date tglAnalisa;
	Date tglBiayaKprLunas;
	Date tglAkad;
	Date createdDate;
	Date updatedDate;
	Boolean isDelete;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdCustomer() {
		return idCustomer;
	}
	public void setIdCustomer(String idCustomer) {
		this.idCustomer = idCustomer;
	}
	public String getIdDeveloper() {
		return idDeveloper;
	}
	public void setIdDeveloper(String idDeveloper) {
		this.idDeveloper = idDeveloper;
	}
	public String getIdBank() {
		return idBank;
	}
	public void setIdBank(String idBank) {
		this.idBank = idBank;
	}
	public Integer getPengajuanDp() {
		return pengajuanDp;
	}
	public void setPengajuanDp(Integer pengajuanDp) {
		this.pengajuanDp = pengajuanDp;
	}
	public Integer getPengajuanAngsuran() {
		return pengajuanAngsuran;
	}
	public void setPengajuanAngsuran(Integer pengajuanAngsuran) {
		this.pengajuanAngsuran = pengajuanAngsuran;
	}
	public Integer getPengajuanTenor() {
		return pengajuanTenor;
	}
	public void setPengajuanTenor(Integer pengajuanTenor) {
		this.pengajuanTenor = pengajuanTenor;
	}
	public Boolean getIsPesanNup() {
		return isPesanNup;
	}
	public void setIsPesanNup(Boolean isPesanNup) {
		this.isPesanNup = isPesanNup;
	}
	public Boolean getIsBerkasLengkap() {
		return isBerkasLengkap;
	}
	public void setIsBerkasLengkap(Boolean isBerkasLengkap) {
		this.isBerkasLengkap = isBerkasLengkap;
	}
	public Boolean getIsAnalisaDisetujui() {
		return isAnalisaDisetujui;
	}
	public void setIsAnalisaDisetujui(Boolean isAnalisaDisetujui) {
		this.isAnalisaDisetujui = isAnalisaDisetujui;
	}
	public Boolean getIsBiayaKprLunas() {
		return isBiayaKprLunas;
	}
	public void setIsBiayaKprLunas(Boolean isBiayaKprLunas) {
		this.isBiayaKprLunas = isBiayaKprLunas;
	}
	public Boolean getIsAkad() {
		return isAkad;
	}
	public void setIsAkad(Boolean isAkad) {
		this.isAkad = isAkad;
	}
	public Date getTglPesanNup() {
		return tglPesanNup;
	}
	public void setTglPesanNup(Date tglPesanNup) {
		this.tglPesanNup = tglPesanNup;
	}
	public Date getTglBerkasMasuk() {
		return tglBerkasMasuk;
	}
	public void setTglBerkasMasuk(Date tglBerkasMasuk) {
		this.tglBerkasMasuk = tglBerkasMasuk;
	}
	public Date getTglAnalisa() {
		return tglAnalisa;
	}
	public void setTglAnalisa(Date tglAnalisa) {
		this.tglAnalisa = tglAnalisa;
	}
	public Date getTglBiayaKprLunas() {
		return tglBiayaKprLunas;
	}
	public void setTglBiayaKprLunas(Date tglBiayaKprLunas) {
		this.tglBiayaKprLunas = tglBiayaKprLunas;
	}
	public Date getTglAkad() {
		return tglAkad;
	}
	public void setTglAkad(Date tglAkad) {
		this.tglAkad = tglAkad;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
	
	
	
}
