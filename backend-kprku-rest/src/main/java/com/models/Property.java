package com.models;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="property")
public class Property {

	String id;
	String idDeveloper;
	String judulProperty;
	Integer hargaUnit;
	Boolean isSyariah;
	Boolean isSubsidi;
	String idPropinsiProperty;
	String idKabupatenProperty;
	String idKecamatanProperty;
	String idKelurahanProperty;
	String kodePosProperty;
	Date createdDate;
	Date updatedDate;
	Boolean isDelete;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdDeveloper() {
		return idDeveloper;
	}
	public void setIdDeveloper(String idDeveloper) {
		this.idDeveloper = idDeveloper;
	}	
	public String getJudulProperty() {
		return judulProperty;
	}
	public void setJudulProperty(String judulProperty) {
		this.judulProperty = judulProperty;
	}
	public Integer getHargaUnit() {
		return hargaUnit;
	}
	public void setHargaUnit(Integer hargaUnit) {
		this.hargaUnit = hargaUnit;
	}
	public Boolean getIsSyariah() {
		return isSyariah;
	}
	public void setIsSyariah(Boolean isSyariah) {
		this.isSyariah = isSyariah;
	}
	public Boolean getIsSubsidi() {
		return isSubsidi;
	}
	public void setIsSubsidi(Boolean isSubsidi) {
		this.isSubsidi = isSubsidi;
	}
	public String getIdPropinsiProperty() {
		return idPropinsiProperty;
	}
	public void setIdPropinsiProperty(String idPropinsiProperty) {
		this.idPropinsiProperty = idPropinsiProperty;
	}
	public String getIdKabupatenProperty() {
		return idKabupatenProperty;
	}
	public void setIdKabupatenProperty(String idKabupatenProperty) {
		this.idKabupatenProperty = idKabupatenProperty;
	}
	public String getIdKecamatanProperty() {
		return idKecamatanProperty;
	}
	public void setIdKecamatanProperty(String idKecamatanProperty) {
		this.idKecamatanProperty = idKecamatanProperty;
	}
	public String getIdKelurahanProperty() {
		return idKelurahanProperty;
	}
	public void setIdKelurahanProperty(String idKelurahanProperty) {
		this.idKelurahanProperty = idKelurahanProperty;
	}
	public String getKodePosProperty() {
		return kodePosProperty;
	}
	public void setKodePosProperty(String kodePosProperty) {
		this.kodePosProperty = kodePosProperty;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}	
}
