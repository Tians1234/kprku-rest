package com.kprku.services;

import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.kprku.configs.MongoConfig;
import com.kprku.utils.ClassReflect;
import com.models.Pembelian;

public class PembelianService {

	ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConfig.class);
	MongoOperations mongo = (MongoOperations) ctx.getBean("mongoTemplate");
	
	public List<Pembelian> getAll(){
		Query query = new Query(Criteria.where("isDelete").is(false));		
		
		return mongo.find(query, Pembelian.class);
	}

	public Pembelian getById(String id) {		
		
		Query query = new Query(Criteria.where("isDelete").is(false).where("_id").is(id));		
		List<Pembelian> lst = mongo.find(query, Pembelian.class);
		
		if(lst.size()>0) { 
			return lst.get(0); 
		}else{
			return new Pembelian();
		}		
	}

	public void insert(Pembelian body) {
		body.setCreatedDate(new Date());
		body.setUpdatedDate(new Date());
		body.setIsDelete(false);
		mongo.save(body);
		
	}

	public void update(Pembelian body) {
		Query query = new Query(Criteria.where("_id").is(body.getId()));			
		
		Update update = new Update();				
		body.setUpdatedDate(new Date());
		ClassReflect classRef = new ClassReflect();
		update = classRef.prosesNotNullOnly(update, body);
		
		mongo.updateFirst(query, update, Pembelian.class);		
	}

	public void delete(String id) {
		Query query = new Query(Criteria.where("_id").is(id));
		Update update = new Update()
				.set("updatedDate", new Date())
				.set("isDelete", true);
		mongo.updateFirst(query, update, Pembelian.class);		
	}
	
}
