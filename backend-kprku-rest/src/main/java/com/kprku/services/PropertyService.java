package com.kprku.services;

import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.kprku.configs.MongoConfig;
import com.kprku.utils.ClassReflect;
import com.models.Property;

public class PropertyService {

	ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConfig.class);
	MongoOperations mongo = (MongoOperations) ctx.getBean("mongoTemplate");
	
	public List<Property> getAll(){
		Query query = new Query(Criteria.where("isDelete").is(false));		
		
		return mongo.find(query, Property.class);
	}

	public Property getById(String id) {		
		Query query = new Query(Criteria.where("isDelete").is(false).where("_id").is(id));		
		List<Property> lst = mongo.find(query, Property.class);		
		
		if(lst.size()>0) { 
			return lst.get(0); 
		}else{
			return new Property();
		}		
	}

	public void insert(Property body) {		
		
		body.setCreatedDate(new Date());
		body.setUpdatedDate(new Date());
		body.setIsDelete(false);
		mongo.save(body);
		
	}

	public void update(Property body) {
		Query query = new Query(Criteria.where("_id").is(body.getId()));
		
		Update update = new Update();		
		body.setUpdatedDate(new Date());
		ClassReflect classRef = new ClassReflect();
		update = classRef.prosesNotNullOnly(update, body);		
		
		mongo.updateFirst(query, update, Property.class);		
	}

	public void delete(String id) {
		Query query = new Query(Criteria.where("_id").is(id));
		Update update = new Update()
				.set("updatedDate", new Date())
				.set("isDelete", true);
		mongo.updateFirst(query, update, Property.class);		
	}

	public List<Property> getByIdDeveloper(String idDeveloper) {
		Query query = new Query(
				Criteria
					.where("idDeveloper").is(idDeveloper)
					.where("isDelete").is(false));		
		
		return mongo.find(query, Property.class);
	}	
	
}
