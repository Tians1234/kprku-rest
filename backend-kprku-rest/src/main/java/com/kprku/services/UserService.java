package com.kprku.services;

import java.util.Date;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.kprku.configs.MongoConfig;
import com.kprku.utils.ClassReflect;
import com.models.User;

@Service
public class UserService {

	ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConfig.class);
	MongoOperations mongo = (MongoOperations) ctx.getBean("mongoTemplate");
	
	public List<User> getAll(){
		Query query = new Query(Criteria.where("isDelete").is(false));		
		
		return mongo.find(query, User.class);
	}

	public User getById(String id) {		
		Query query = new Query(Criteria.where("isDelete").is(false).where("_id").is(id));		
		List<User> lst = mongo.find(query, User.class);		
		
		if(lst.size()>0) { 
			return lst.get(0); 
		}else{
			return new User();
		}		
	}

	public void insert(User body) {
		body.setPassword(BCrypt.hashpw(body.getPassword(), BCrypt.gensalt())); //password di decrypt disini
		body.setCreatedDate(new Date());
		body.setUpdateDate(new Date());
		body.setIsDelete(false);
		mongo.save(body);
		
	}

	public void update(User body) {
		Query query = new Query(Criteria.where("_id").is(body.getId()));
		
		Update update = new Update();					
		body.setUpdateDate(new Date());		
		ClassReflect classRef = new ClassReflect();
		update = classRef.prosesNotNullOnly(update, body);		
		
		mongo.updateFirst(query, update, User.class);		
	}

	public void delete(String id) {
		Query query = new Query(Criteria.where("_id").is(id));
		Update update = new Update()
				.set("updateDate", new Date())
				.set("isDelete", true);
		mongo.updateFirst(query, update, User.class);		
	}
	
	public User getUserByLogin(String username, String password) {
		
		Query query = new Query(Criteria.where("username").is(username));
		List<User> lst = mongo.find(query, User.class);		
		
		for (User user : lst) {					
			if(BCrypt.checkpw(password, user.getPassword())) {				
				return user;
			}			
		}
		
		return null;
	}
	
}
