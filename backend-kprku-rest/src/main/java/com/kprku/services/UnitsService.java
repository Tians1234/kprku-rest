package com.kprku.services;

import java.util.Date;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.RequestParam;

import com.kprku.configs.MongoConfig;
import com.kprku.utils.ClassReflect;
import com.models.Units;

public class UnitsService {

	ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConfig.class);
	MongoOperations mongo = (MongoOperations) ctx.getBean("mongoTemplate");
	
	public List<Units> getAll(){
		Query query = new Query(Criteria.where("isDelete").is(false));		
		
		return mongo.find(query, Units.class);
	}

	public Units getById(String id) {		
		Query query = new Query(Criteria.where("isDelete").is(false).where("_id").is(id));		
		List<Units> lst = mongo.find(query, Units.class);		
		
		if(lst.size()>0) { 
			return lst.get(0); 
		}else{
			return new Units();
		}		
	}

	public void insert(Units body) {		
		
		body.setCreatedDate(new Date());
		body.setUpdatedDate(new Date());
		body.setIsDelete(false);
		mongo.save(body);
		
	}

	public void update(Units body) {
		Query query = new Query(Criteria.where("_id").is(body.getId()));		
	
		Update update = new Update();					
		body.setUpdatedDate(new Date());
		ClassReflect classRef = new ClassReflect();
		update = classRef.prosesNotNullOnly(update, body);	
		
		mongo.updateFirst(query, update, Units.class);		
	}

	public void delete(String id) {
		Query query = new Query(Criteria.where("_id").is(id));
		Update update = new Update()
				.set("updatedDate", new Date())
				.set("isDelete", true);
		mongo.updateFirst(query, update, Units.class);		
	}

	public List<Units> getUnitsByIdProperty(String idProperty) {
		Query query = new Query(Criteria.where("idProperty").is(idProperty).where("isDelete").is(false));
		return mongo.find(query, Units.class);
	}	
}
