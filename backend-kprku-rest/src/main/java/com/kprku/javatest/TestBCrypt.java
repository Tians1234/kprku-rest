package com.kprku.javatest;

import org.mindrot.jbcrypt.BCrypt;

public class TestBCrypt {

	public static void main(String[] args) {
		for(Integer i=0;i<10;i++) {
			String password = "ikea123";
			String salt = BCrypt.gensalt();
			String hash = BCrypt.hashpw(password, salt);
			
			System.out.println("Hash : " + hash);
			System.out.println("Salt : " + salt);
			
			System.out.println("Un Hash " + BCrypt.checkpw(password, hash));
		}
	}

}
