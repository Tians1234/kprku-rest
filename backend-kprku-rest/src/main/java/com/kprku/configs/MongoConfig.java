package com.kprku.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;

@Configuration
public class MongoConfig {

	@Bean
	public MongoDbFactory mongoDbFactory() throws Exception {		
		return new SimpleMongoDbFactory(new MongoClient(), "kprku");
	}
	
	@Bean
	public MongoTemplate mongoTemplate()  throws Exception {
		return new MongoTemplate(this.mongoDbFactory());
	}
}
