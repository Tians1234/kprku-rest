package com.kprku.controllers;

import java.text.ParseException;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kprku.services.CustomerService;
import com.models.Customer;

@Validated
@RestController
public class CustomerController {

	CustomerService service = new CustomerService();

	@RequestMapping(value="/customer", method=RequestMethod.GET)
	public Object get(
			@RequestParam(value="id", defaultValue="") String id
			) {
		if(!id.isEmpty()) { // bila id TIDAK kosong
			return service.getById(id); // respon JSON sesuai parameter ID
		}else {
			return service.getAll(); // respon JSON semua list
		}
		
	}
	
	@RequestMapping(value="/customer", method=RequestMethod.POST)
	public Object post(
			Customer customer
			) {		
		
		try {
			service.insert(customer);
			return "POST OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/customer", method=RequestMethod.PUT)
	public Object put(
			Customer customer		
			) {
		
		try {
			service.update(customer);
			return "PUT OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/customer", method=RequestMethod.DELETE)
	public Object delete(
			@RequestParam(value="id", defaultValue="")String id			
			) {		
		
		try {
			service.delete(id);
			return "DELETE OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
