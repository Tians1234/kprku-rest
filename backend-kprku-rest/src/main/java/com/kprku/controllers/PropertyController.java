package com.kprku.controllers;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kprku.services.PropertyService;
import com.kprku.services.UserService;
import com.models.Property;

@Validated
@RestController
public class PropertyController {

	PropertyService service = new PropertyService();
		
	@RequestMapping(value="/property", method=RequestMethod.GET)
	public Object get(
			@RequestParam(value="id", defaultValue="") String id,
			@RequestParam(value="idDeveloper", defaultValue="") String idDeveloper
			) {
		
		if(!idDeveloper.isEmpty()) {
			return service.getByIdDeveloper(idDeveloper);
		}
		
		if(!id.isEmpty()) { // bila id TIDAK kosong
			return service.getById(id); // respon JSON sesuai parameter ID
		}else {
			return service.getAll(); // respon JSON semua list
		}
		
	}
	
	@RequestMapping(value="/property", method=RequestMethod.POST)
	public Object post(
			Property property
			) {
		
		try {
			service.insert(property);
			return "POST OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/property", method=RequestMethod.PUT)
	public Object put(
			Property property
			) {
				
		try {
			service.update(property);
			return "PUT OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/property", method=RequestMethod.DELETE)
	public Object delete(
			@RequestParam(value="id", defaultValue="")String id			
			) {		
		
		try {
			service.delete(id);
			return "DELETE OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
