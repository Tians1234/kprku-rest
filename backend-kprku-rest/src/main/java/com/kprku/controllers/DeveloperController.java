package com.kprku.controllers;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.kprku.services.DeveloperService;
import com.models.Developer;

@Validated
@RestController
public class DeveloperController {

	DeveloperService service = new DeveloperService();
	
	@RequestMapping(value="/developer", method=RequestMethod.GET)
	public Object get(
			@RequestParam(value="id", defaultValue="") String id
			) {
		if(!id.isEmpty()) { // bila id TIDAK kosong
			return service.getById(id); // respon JSON sesuai parameter ID
		}else {
			return service.getAll(); // respon JSON semua list
		}
		
	}
	
	@RequestMapping(value="/developer", method=RequestMethod.POST)
	public Object post(			
			Developer developer
			) {			
		
		try {
			service.insert(developer);
			return "POST OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/developer", method=RequestMethod.PUT)
	public Object put(
			Developer developer
			) {
		
		try {
			service.update(developer);
			return "PUT OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/developer", method=RequestMethod.DELETE)
	public Object delete(
			@RequestParam(value="id", defaultValue="")String id			
			) {		
		
		try {
			service.delete(id);
			return "DELETE OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
