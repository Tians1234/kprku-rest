package com.kprku.controllers;

import java.lang.reflect.Field;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kprku.services.BankService;
import com.models.Bank;

@Validated
@RestController
public class BankController {

	// by chris
	
	BankService service = new BankService();

	@RequestMapping(value="/bank", method=RequestMethod.GET)
	public Object get(
			@RequestParam(value="id", defaultValue="") String id
			) {
		if(!id.isEmpty()) { // bila id TIDAK kosong
			return service.getById(id); // respon JSON sesuai parameter ID
		}else {
			return service.getAll(); // respon JSON semua list
		}
		
	}
	
	@RequestMapping(value="/bank", method=RequestMethod.POST)
	public Object post(
			Bank bank
			) {		
		
		try {
			service.insert(bank);
			return "POST OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/bank", method=RequestMethod.PUT)
	public Object put(
			Bank bank							
			) {		
		
		try {
			service.update(bank);
			return "PUT OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/bank", method=RequestMethod.DELETE)
	public Object delete(
			@RequestParam(value="id", defaultValue="")String id			
			) {		
		
		try {
			service.delete(id);
			return "DELETE OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
