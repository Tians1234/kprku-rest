package com.kprku.controllers;

import java.text.ParseException;
import java.util.Date;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kprku.services.PembelianService;
import com.models.Pembelian;

@Validated
@RestController
public class PembelianController {

PembelianService service = new PembelianService();	
	
	@RequestMapping(value="/pembelian", method=RequestMethod.GET)
	public Object get(
			@RequestParam(value="id", defaultValue="") String id			
			) {		
		
		if(!id.isEmpty()) { // bila id TIDAK kosong
			return service.getById(id); // respon JSON sesuai parameter ID
		}else {
			return service.getAll(); // respon JSON semua list
		}
		
	}
	
	@RequestMapping(value="/pembelian", method=RequestMethod.POST)
	public Object post(
			Pembelian pembelian				
			)  {			
		
		try {
			service.insert(pembelian);
			return "POST OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/pembelian", method=RequestMethod.PUT)
	public Object put(
			Pembelian pembelian						
			) {
		
		try {
			service.update(pembelian);
			return "PUT OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/pembelian", method=RequestMethod.DELETE)
	public Object delete(
			@RequestParam(value="id", defaultValue="")String id			
			) {		
		
		try {
			service.delete(id);
			return "DELETE OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
