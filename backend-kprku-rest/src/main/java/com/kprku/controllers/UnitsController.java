package com.kprku.controllers;

import java.text.ParseException;
import java.util.Date;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kprku.services.UnitsService;
import com.models.Units;

@Validated
@RestController
public class UnitsController {

	UnitsService service = new UnitsService();	
	
	@RequestMapping(value="/units", method=RequestMethod.GET)
	public Object get(
			@RequestParam(value="id", defaultValue="") String id,
			@RequestParam(value="idProperty", defaultValue="") String idProperty
			) {
		if(!idProperty.isEmpty()) {
			return service.getUnitsByIdProperty(idProperty);
		}
		
		if(!id.isEmpty()) { // bila id TIDAK kosong
			return service.getById(id); // respon JSON sesuai parameter ID
		}else {
			return service.getAll(); // respon JSON semua list
		}
		
	}
	
	@RequestMapping(value="/units", method=RequestMethod.POST)
	public Object post(
			Units units	
			) {
				
		try {
			service.insert(units);
			return "POST OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/units", method=RequestMethod.PUT)
	public Object put(
			Units units
			) {		
		
		try {
			service.update(units);
			return "PUT OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/units", method=RequestMethod.DELETE)
	public Object delete(
			@RequestParam(value="id", defaultValue="")String id			
			) {		
		
		try {
			service.delete(id);
			return "DELETE OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
