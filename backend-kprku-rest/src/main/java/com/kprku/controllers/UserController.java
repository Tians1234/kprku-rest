package com.kprku.controllers;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kprku.services.UserService;
import com.models.User;

@Validated
@RestController
public class UserController {

	UserService service = new UserService();
	
	@RequestMapping(value="/userlogin", method=RequestMethod.GET)
	public Object getUserLogin(
			@RequestParam(value="username", defaultValue="") String username,
			@RequestParam(value="password", defaultValue="") String password
			) {
		
		// periksa apakah username dan password valid
		User user = service.getUserByLogin(username, password);
		
		return user; // bila user null berarti username dan password tidak ditemukan
	}
	
	@RequestMapping(value="/user", method=RequestMethod.GET)
	public Object get(
			@RequestParam(value="id", defaultValue="") String id
			) {
		if(!id.isEmpty()) { // bila id TIDAK kosong
			return service.getById(id); // respon JSON sesuai parameter ID
		}else {
			return service.getAll(); // respon JSON semua list
		}
		
	}
	
	@RequestMapping(value="/user", method=RequestMethod.POST)
	public Object post(
			User user
			) {
			
		try {
			service.insert(user);
			return "POST OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/user", method=RequestMethod.PUT)
	public Object put(
			User user
			) {
			if(user.getPassword()!=null) {user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));}
		try {
			service.update(user);
			return "PUT OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/user", method=RequestMethod.DELETE)
	public Object delete(
			@RequestParam(value="id", defaultValue="")String id			
			) {		
		
		try {
			service.delete(id);
			return "DELETE OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
